" Automatic reloading of .vimrc
autocmd! bufwritepost .vimrc source %

" Show whitespace
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
au InsertLeave * match ExtraWhitespace /\s\+$/

filetype off
filetype plugin indent on
syntax on

" color sierra
" color monokai
" color jellybeans
" color brogrammer
" neodark
" let g:neodark#background='brown'
" colorscheme neodark

" color railscasts
" color evening
" color guardian
" color slate
" color solarized
" color wwdc16
" color seoul256
" color badwolf
" color deep-space
" color two-firewatch
" color lucario
" color moriarty
" colorscheme monokai
" set t_Co=256
" colorscheme mohammad
" colorscheme loogica
" colorscheme PaperColor
" colorscheme amcolors
" colorscheme happy_hacking
" colorscheme tender
" let macvim_skip_colorscheme=1
" set background=dark
" colorscheme quantum
" let g:quantum_black = 1

" let g:despacio_Campfire = 1
" colorscheme despacio

if has("termguicolors")
	set termguicolors
endif

" colorscheme despacio
" let g:despacio_Campfire = 1
" set background=dark
colorscheme molokai

" set guifont=Droid\ Sans\ Mono\ for\ Powerline:h14
" set guifont=Roboto\ Mono\ Bold\ for\ Powerline:h14
set guifont=Go\ Mono:h14
" set guifont=Source\ Code\ Pro\ for\ Powerline:h15
" set guifont=Droid\ Sans\ Mono\ for\ Powerline:h13
set cindent

set number
set showcmd
set wrap

set tabstop=4
set noexpandtab
" set softtabstop=4
set shiftwidth=4
set shiftround
" set expandtab
set cc=80

set hlsearch
set incsearch
set ignorecase
set smartcase

set nobackup
set nowritebackup
set noswapfile

call pathogen#infect()

set laststatus=2

execute pathogen#infect()

let g:ctrlp_max_height = 15
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=*/coverage/*
set wildignore+=*/env/*
set wildignore+=*/node_modules/*
set wildignore+=*/composer/*

let g:go_fmt_command = "goimports"
let g:go_term_mode = "split"
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
" let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
" let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go', 'php'] }
let g:go_list_type = "quickfix"

" map <leader>sw<left>  :topleft  vnew<CR>

autocmd BufWritePre * :%s/\s\+$//e

":nmap <c-l> :%s/^\s*/&&
map <F7> mzgg=G`w
map <space> viw
"let g:sierra_Campfire = 1
" map <c-s-n> :rightbelow vnew<enter>
map <c-s-n> :rightbelow new<enter>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
map <F5> mz:e!<enter>
highlight ColorColumn ctermbg=red
" set mouse=""
set mouse-=a
nmap gb :GoBuild<CR>
nmap <D-e> :Explore<CR>
" let g:airline_powerline_fonts = 1
" let g:airline_theme = 'base16_3024'
" let g:airline_theme = 'deep_space'
" let g:airline_theme = 'flattown'
let g:airline_theme = 'papercolor'
" let g:airline_them = 'tender'
" let g:airline_theme = 'quantum'

"disable scrollbar
set guioptions-=r
set guioptions-=T

noremap <Up> :!say Tai Ngongo<CR><CR>
noremap <Down> :!say Tai Ngongo<CR><CR>
noremap <Right> :!say Tai Ngongo<CR><CR>
noremap <Left> :!say Tai Ngongo<CR><CR>

imap <up> <nop>
imap <down> <nop>
imap <right> <nop>
imap <left> <nop>

nmap gy :Goyo<CR>
let g:goyo_width = 85
let g:goyo_height = 90
let g:goyo_linenr = 1

let g:netrw_list_hide='.*\.swp$,.*\.pyc,.*\.idea,.*\.git,.*\.phalcon,.*\.phpintel'

" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" map <C-p> :NERDTreeToggle<CR>
set cursorline

if has("gui_macvim")
	set guioptions-=m  "no menu
	set guioptions-=T  "no toolbar
	set guioptions-=l
	set guioptions-=L
	set guioptions-=r  "no scrollbar
	set guioptions-=R
	" let macvim_skip_colorscheme=1
	nmap <D-]> >>
	nmap <D-[> <<
	vmap <D-[> <gv
	vmap <D-]> >gv
	let g:ctrlp_map = '<D-p>'
	let g:ctrlp_cmd = 'CtrlP'
endif

let g:jsx_ext_required = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:multi_cursor_next_key='<D-d>'
let g:javascript_plugin_jsdoc = 1
